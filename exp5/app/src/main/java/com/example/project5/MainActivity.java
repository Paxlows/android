package com.example.project5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button B;
    EditText e;
    String s;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        B = (Button) findViewById(R.id.button);
        e = (EditText) findViewById(R.id.edittext);
        B.setOnClickListener((v) -> {
                    Intent i = new Intent(MainActivity.this, MainActivity2.class);
                    s = e.getText().toString();
                    i.putExtra("name", s);
                    startActivity(i);
                });
}
}