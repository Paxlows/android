package com.example.experiment7listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final String student[]={"ABHI", "ARUN","JIS"};
        final ListView lt;
        lt=(ListView)findViewById(R.id.lt1);
        ArrayAdapter adapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,student);
        lt.setAdapter(adapter);
        lt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String val=(String)lt.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(),val,Toast.LENGTH_SHORT).show();
            }
        });
        }
}
