package com.example.exeperiment8sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button B1,B2;
    EditText E;
    SharedPreferences SP;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        B1=(Button)findViewById(R.id.button);
        B2=(Button)findViewById(R.id.button2);
        E=(EditText)findViewById(R.id.editText);
        SP=getSharedPreferences("pref",MODE_PRIVATE);
        B1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s=E.getText().toString();
                editor=SP.edit();
                editor.putString("msg",s);
                editor.commit();
                Toast.makeText(MainActivity.this,"Message saved successfully",Toast.LENGTH_LONG);
            }
        });
        B2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this,Main2Activity.class);
                startActivity(i);
                if (SP.contains("msg"))
                {
                    String s=SP.getString("msg","");
                    Toast.makeText(MainActivity.this,s,Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}
